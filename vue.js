new Vue({
el:'#app',
data(){
    return{
        layers:[],
        hi:'hello world',
        defaultType:'baze',
        defaultHeight:5,
        layersTypes:{
            brick:{
             label:'brick',
             price1sm:20,
            },
            wallpaper:{
            label:'wallpaper',
            price1sm:30,
            },
            baze:{
            label:'baze',
            price1sm:40,
            }
        }
    }
},computed:{
    price(){
        let sum=0
        this.layers.forEach((layer)=> {
             sum+=layer.height*this.layersTypes[layer.type].price1sm
        });
        return sum
      },
      hasLayers(){
          return this.layers.length >0 
      }
},
methods:{
    add(){
        this.layers.push({
            type:this.defaultType,
            height:this.defaultHeight
        })
    },
    changeKey(i,dh){
     this.layers[i].height+=dh;
    },
    delateL(i){
        this.layers.splice(i,1)
    }
}
})